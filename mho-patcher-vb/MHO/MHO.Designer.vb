﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MHO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MHO))
        Me.StartPatch = New System.Windows.Forms.Button()
        Me.logbox = New System.Windows.Forms.RichTextBox()
        Me.selectGameFolderPath = New System.Windows.Forms.FolderBrowserDialog()
        Me.MHOGameClientTicker = New System.Windows.Forms.Timer(Me.components)
        Me.versionLabel = New System.Windows.Forms.Label()
        Me.gamerunningok = New System.Windows.Forms.Label()
        Me.mhostatus = New System.Windows.Forms.Label()
        Me.gamefolderok = New System.Windows.Forms.Label()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AutorunPatchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinimizeToTrayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ChangeGameFolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CPCGAMEasdasdasdToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.StartPatchToolStrip = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VersionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.gameversiontxt = New System.Windows.Forms.ToolStripMenuItem()
        Me.patchversiontxt = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacebookToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.DiscordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WikiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RedditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DonateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PatreonToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaypalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitcoinToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VersionsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.gameversiontxtcontext = New System.Windows.Forms.ToolStripMenuItem()
        Me.patchversiontxtcontext = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiscordToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.WikiToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RedditToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbouToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ShowToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiscordImage = New System.Windows.Forms.PictureBox()
        Me.kenma9123Text = New System.Windows.Forms.Label()
        Me.patchhookok = New System.Windows.Forms.Label()
        Me.patchhookStatus = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.gamefolderStatus = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.PatreonImage = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.DiscordImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PatreonImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StartPatch
        '
        Me.StartPatch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.StartPatch.Enabled = False
        Me.StartPatch.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.StartPatch.Location = New System.Drawing.Point(14, 224)
        Me.StartPatch.Name = "StartPatch"
        Me.StartPatch.Size = New System.Drawing.Size(313, 52)
        Me.StartPatch.TabIndex = 0
        Me.StartPatch.Text = "START"
        Me.StartPatch.UseVisualStyleBackColor = True
        '
        'logbox
        '
        Me.logbox.Location = New System.Drawing.Point(14, 85)
        Me.logbox.Name = "logbox"
        Me.logbox.ReadOnly = True
        Me.logbox.Size = New System.Drawing.Size(313, 136)
        Me.logbox.TabIndex = 1
        Me.logbox.Text = "Preparing files..."
        '
        'selectGameFolderPath
        '
        Me.selectGameFolderPath.Description = "Select Monster Hunter Online folder"
        '
        'MHOGameClientTicker
        '
        Me.MHOGameClientTicker.Interval = 1000
        '
        'versionLabel
        '
        Me.versionLabel.AutoSize = True
        Me.versionLabel.Location = New System.Drawing.Point(12, 279)
        Me.versionLabel.Name = "versionLabel"
        Me.versionLabel.Size = New System.Drawing.Size(29, 13)
        Me.versionLabel.TabIndex = 16
        Me.versionLabel.Text = "label"
        '
        'gamerunningok
        '
        Me.gamerunningok.AutoSize = True
        Me.gamerunningok.Enabled = False
        Me.gamerunningok.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.gamerunningok.ForeColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.gamerunningok.Location = New System.Drawing.Point(297, 60)
        Me.gamerunningok.Name = "gamerunningok"
        Me.gamerunningok.Size = New System.Drawing.Size(30, 17)
        Me.gamerunningok.TabIndex = 21
        Me.gamerunningok.Text = "OK"
        '
        'mhostatus
        '
        Me.mhostatus.AutoSize = True
        Me.mhostatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.mhostatus.Location = New System.Drawing.Point(168, 58)
        Me.mhostatus.Name = "mhostatus"
        Me.mhostatus.Size = New System.Drawing.Size(126, 18)
        Me.mhostatus.TabIndex = 15
        Me.mhostatus.Text = "GAME RUNNING"
        '
        'gamefolderok
        '
        Me.gamefolderok.AutoSize = True
        Me.gamefolderok.Enabled = False
        Me.gamefolderok.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.gamefolderok.ForeColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.gamefolderok.Location = New System.Drawing.Point(297, 33)
        Me.gamefolderok.Name = "gamefolderok"
        Me.gamefolderok.Size = New System.Drawing.Size(30, 17)
        Me.gamefolderok.TabIndex = 19
        Me.gamefolderok.Text = "OK"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OptionsToolStripMenuItem, Me.ToolStripSeparator2, Me.StartPatchToolStrip, Me.ExitToolStripMenuItem1})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Tag = ""
        Me.FileToolStripMenuItem.Text = "File"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AutorunPatchToolStripMenuItem, Me.MinimizeToTrayToolStripMenuItem, Me.ToolStripSeparator1, Me.ChangeGameFolderToolStripMenuItem, Me.CPCGAMEasdasdasdToolStripMenuItem})
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.OptionsToolStripMenuItem.Text = "Preferences"
        '
        'AutorunPatchToolStripMenuItem
        '
        Me.AutorunPatchToolStripMenuItem.Name = "AutorunPatchToolStripMenuItem"
        Me.AutorunPatchToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.AutorunPatchToolStripMenuItem.Size = New System.Drawing.Size(235, 22)
        Me.AutorunPatchToolStripMenuItem.Text = "Autostart Patch"
        Me.AutorunPatchToolStripMenuItem.ToolTipText = "Patch will auto start on launcher load"
        '
        'MinimizeToTrayToolStripMenuItem
        '
        Me.MinimizeToTrayToolStripMenuItem.Name = "MinimizeToTrayToolStripMenuItem"
        Me.MinimizeToTrayToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.MinimizeToTrayToolStripMenuItem.Size = New System.Drawing.Size(235, 22)
        Me.MinimizeToTrayToolStripMenuItem.Text = "Minimize to Tray"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(232, 6)
        '
        'ChangeGameFolderToolStripMenuItem
        '
        Me.ChangeGameFolderToolStripMenuItem.AutoToolTip = True
        Me.ChangeGameFolderToolStripMenuItem.Name = "ChangeGameFolderToolStripMenuItem"
        Me.ChangeGameFolderToolStripMenuItem.ShortcutKeyDisplayString = ""
        Me.ChangeGameFolderToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.ChangeGameFolderToolStripMenuItem.Size = New System.Drawing.Size(235, 22)
        Me.ChangeGameFolderToolStripMenuItem.Text = "Game Folder  ..."
        '
        'CPCGAMEasdasdasdToolStripMenuItem
        '
        Me.CPCGAMEasdasdasdToolStripMenuItem.Enabled = False
        Me.CPCGAMEasdasdasdToolStripMenuItem.Name = "CPCGAMEasdasdasdToolStripMenuItem"
        Me.CPCGAMEasdasdasdToolStripMenuItem.Size = New System.Drawing.Size(235, 22)
        Me.CPCGAMEasdasdasdToolStripMenuItem.Text = "..."
        Me.CPCGAMEasdasdasdToolStripMenuItem.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(168, 6)
        '
        'StartPatchToolStrip
        '
        Me.StartPatchToolStrip.Enabled = False
        Me.StartPatchToolStrip.Name = "StartPatchToolStrip"
        Me.StartPatchToolStrip.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.StartPatchToolStrip.Size = New System.Drawing.Size(171, 22)
        Me.StartPatchToolStrip.Text = "Start Patch"
        '
        'ExitToolStripMenuItem1
        '
        Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
        Me.ExitToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(171, 22)
        Me.ExitToolStripMenuItem1.Text = "Exit"
        '
        'VersionsToolStripMenuItem
        '
        Me.VersionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.gameversiontxt, Me.patchversiontxt})
        Me.VersionsToolStripMenuItem.Name = "VersionsToolStripMenuItem"
        Me.VersionsToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.VersionsToolStripMenuItem.Text = "Versions"
        '
        'gameversiontxt
        '
        Me.gameversiontxt.Name = "gameversiontxt"
        Me.gameversiontxt.Size = New System.Drawing.Size(152, 22)
        Me.gameversiontxt.Text = "Game version: "
        '
        'patchversiontxt
        '
        Me.patchversiontxt.Name = "patchversiontxt"
        Me.patchversiontxt.Size = New System.Drawing.Size(152, 22)
        Me.patchversiontxt.Text = "Patch version:"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacebookToolStripMenuItem, Me.ToolStripSeparator4, Me.DiscordToolStripMenuItem, Me.WikiToolStripMenuItem, Me.RedditToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'FacebookToolStripMenuItem
        '
        Me.FacebookToolStripMenuItem.Name = "FacebookToolStripMenuItem"
        Me.FacebookToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.FacebookToolStripMenuItem.Text = "Facebook"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(122, 6)
        '
        'DiscordToolStripMenuItem
        '
        Me.DiscordToolStripMenuItem.Name = "DiscordToolStripMenuItem"
        Me.DiscordToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.DiscordToolStripMenuItem.Text = "Discord"
        '
        'WikiToolStripMenuItem
        '
        Me.WikiToolStripMenuItem.Name = "WikiToolStripMenuItem"
        Me.WikiToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.WikiToolStripMenuItem.Text = "Wiki"
        '
        'RedditToolStripMenuItem
        '
        Me.RedditToolStripMenuItem.Name = "RedditToolStripMenuItem"
        Me.RedditToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.RedditToolStripMenuItem.Text = "Reddit"
        '
        'AboutToolStripMenuItem1
        '
        Me.AboutToolStripMenuItem1.Name = "AboutToolStripMenuItem1"
        Me.AboutToolStripMenuItem1.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem1.Text = "About"
        '
        'DonateToolStripMenuItem
        '
        Me.DonateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PatreonToolStripMenuItem, Me.PaypalToolStripMenuItem, Me.BitcoinToolStripMenuItem})
        Me.DonateToolStripMenuItem.Name = "DonateToolStripMenuItem"
        Me.DonateToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.DonateToolStripMenuItem.Text = "Support Us"
        '
        'PatreonToolStripMenuItem
        '
        Me.PatreonToolStripMenuItem.Name = "PatreonToolStripMenuItem"
        Me.PatreonToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.PatreonToolStripMenuItem.Text = "Patreon"
        '
        'PaypalToolStripMenuItem
        '
        Me.PaypalToolStripMenuItem.Name = "PaypalToolStripMenuItem"
        Me.PaypalToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.PaypalToolStripMenuItem.Text = "Paypal"
        '
        'BitcoinToolStripMenuItem
        '
        Me.BitcoinToolStripMenuItem.Name = "BitcoinToolStripMenuItem"
        Me.BitcoinToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.BitcoinToolStripMenuItem.Text = "Bitcoin"
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.NotifyIcon1.BalloonTipTitle = "MHO - English Patch"
        Me.NotifyIcon1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "MHO - English Patch"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VersionsToolStripMenuItem1, Me.HelpToolStripMenuItem1, Me.AbouToolStripMenuItem, Me.ToolStripSeparator3, Me.ShowToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(118, 120)
        '
        'VersionsToolStripMenuItem1
        '
        Me.VersionsToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.gameversiontxtcontext, Me.patchversiontxtcontext})
        Me.VersionsToolStripMenuItem1.Name = "VersionsToolStripMenuItem1"
        Me.VersionsToolStripMenuItem1.Size = New System.Drawing.Size(117, 22)
        Me.VersionsToolStripMenuItem1.Text = "Versions"
        '
        'gameversiontxtcontext
        '
        Me.gameversiontxtcontext.Name = "gameversiontxtcontext"
        Me.gameversiontxtcontext.Size = New System.Drawing.Size(149, 22)
        Me.gameversiontxtcontext.Text = "Game version:"
        '
        'patchversiontxtcontext
        '
        Me.patchversiontxtcontext.Name = "patchversiontxtcontext"
        Me.patchversiontxtcontext.Size = New System.Drawing.Size(149, 22)
        Me.patchversiontxtcontext.Text = "Patch version:"
        '
        'HelpToolStripMenuItem1
        '
        Me.HelpToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiscordToolStripMenuItem1, Me.WikiToolStripMenuItem1, Me.RedditToolStripMenuItem1})
        Me.HelpToolStripMenuItem1.Name = "HelpToolStripMenuItem1"
        Me.HelpToolStripMenuItem1.Size = New System.Drawing.Size(117, 22)
        Me.HelpToolStripMenuItem1.Text = "Help"
        '
        'DiscordToolStripMenuItem1
        '
        Me.DiscordToolStripMenuItem1.Name = "DiscordToolStripMenuItem1"
        Me.DiscordToolStripMenuItem1.Size = New System.Drawing.Size(114, 22)
        Me.DiscordToolStripMenuItem1.Text = "Discord"
        '
        'WikiToolStripMenuItem1
        '
        Me.WikiToolStripMenuItem1.Name = "WikiToolStripMenuItem1"
        Me.WikiToolStripMenuItem1.Size = New System.Drawing.Size(114, 22)
        Me.WikiToolStripMenuItem1.Text = "Wiki"
        '
        'RedditToolStripMenuItem1
        '
        Me.RedditToolStripMenuItem1.Name = "RedditToolStripMenuItem1"
        Me.RedditToolStripMenuItem1.Size = New System.Drawing.Size(114, 22)
        Me.RedditToolStripMenuItem1.Text = "Reddit"
        '
        'AbouToolStripMenuItem
        '
        Me.AbouToolStripMenuItem.Name = "AbouToolStripMenuItem"
        Me.AbouToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.AbouToolStripMenuItem.Text = "About"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(114, 6)
        '
        'ShowToolStripMenuItem
        '
        Me.ShowToolStripMenuItem.Name = "ShowToolStripMenuItem"
        Me.ShowToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.ShowToolStripMenuItem.Text = "Show"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'DiscordImage
        '
        Me.DiscordImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DiscordImage.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DiscordImage.Image = Global.MHO_Patch.My.Resources.Resources.discordbutton
        Me.DiscordImage.Location = New System.Drawing.Point(333, 224)
        Me.DiscordImage.Name = "DiscordImage"
        Me.DiscordImage.Size = New System.Drawing.Size(129, 52)
        Me.DiscordImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.DiscordImage.TabIndex = 29
        Me.DiscordImage.TabStop = False
        '
        'kenma9123Text
        '
        Me.kenma9123Text.AutoSize = True
        Me.kenma9123Text.Enabled = False
        Me.kenma9123Text.Location = New System.Drawing.Point(527, 279)
        Me.kenma9123Text.Name = "kenma9123Text"
        Me.kenma9123Text.Size = New System.Drawing.Size(63, 13)
        Me.kenma9123Text.TabIndex = 31
        Me.kenma9123Text.Text = "kenma9123"
        '
        'patchhookok
        '
        Me.patchhookok.AutoSize = True
        Me.patchhookok.Enabled = False
        Me.patchhookok.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.patchhookok.ForeColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.patchhookok.Location = New System.Drawing.Point(135, 60)
        Me.patchhookok.Name = "patchhookok"
        Me.patchhookok.Size = New System.Drawing.Size(30, 17)
        Me.patchhookok.TabIndex = 33
        Me.patchhookok.Text = "OK"
        '
        'patchhookStatus
        '
        Me.patchhookStatus.AutoSize = True
        Me.patchhookStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.patchhookStatus.Location = New System.Drawing.Point(12, 58)
        Me.patchhookStatus.Name = "patchhookStatus"
        Me.patchhookStatus.Size = New System.Drawing.Size(107, 18)
        Me.patchhookStatus.TabIndex = 32
        Me.patchhookStatus.Text = "PATCH HOOK"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(334, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(251, 131)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = resources.GetString("Label1.Text")
        Me.Label1.UseCompatibleTextRendering = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(135, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 17)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "OK"
        '
        'gamefolderStatus
        '
        Me.gamefolderStatus.AutoSize = True
        Me.gamefolderStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.gamefolderStatus.Location = New System.Drawing.Point(168, 33)
        Me.gamefolderStatus.Name = "gamefolderStatus"
        Me.gamefolderStatus.Size = New System.Drawing.Size(117, 18)
        Me.gamefolderStatus.TabIndex = 17
        Me.gamefolderStatus.Text = "GAME FOLDER"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 18)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "PATCH LOADER"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.VersionsToolStripMenuItem, Me.DonateToolStripMenuItem, Me.HelpToolStripMenuItem, Me.AboutToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(602, 24)
        Me.MenuStrip1.TabIndex = 22
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'PatreonImage
        '
        Me.PatreonImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PatreonImage.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PatreonImage.Image = Global.MHO_Patch.My.Resources.Resources.patreonbutton
        Me.PatreonImage.Location = New System.Drawing.Point(468, 224)
        Me.PatreonImage.Name = "PatreonImage"
        Me.PatreonImage.Size = New System.Drawing.Size(119, 52)
        Me.PatreonImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PatreonImage.TabIndex = 28
        Me.PatreonImage.TabStop = False
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(332, 173)
        Me.Button1.Margin = New System.Windows.Forms.Padding(0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(256, 45)
        Me.Button1.TabIndex = 34
        Me.Button1.Text = "Our Patrons"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'MHO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(602, 304)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.patchhookok)
        Me.Controls.Add(Me.patchhookStatus)
        Me.Controls.Add(Me.kenma9123Text)
        Me.Controls.Add(Me.DiscordImage)
        Me.Controls.Add(Me.PatreonImage)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.gamerunningok)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.gamefolderok)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.gamefolderStatus)
        Me.Controls.Add(Me.versionLabel)
        Me.Controls.Add(Me.mhostatus)
        Me.Controls.Add(Me.logbox)
        Me.Controls.Add(Me.StartPatch)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MHO"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Monster Hunter Online - English Patch"
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.DiscordImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PatreonImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StartPatch As System.Windows.Forms.Button
    Public WithEvents logbox As System.Windows.Forms.RichTextBox
    Friend WithEvents selectGameFolderPath As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents MHOGameClientTicker As System.Windows.Forms.Timer
    Friend WithEvents versionLabel As System.Windows.Forms.Label
    Friend WithEvents gamerunningok As System.Windows.Forms.Label
    Friend WithEvents mhostatus As System.Windows.Forms.Label
    Friend WithEvents gamefolderok As System.Windows.Forms.Label
    Friend WithEvents VersionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gameversiontxt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents patchversiontxt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StartPatchToolStrip As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiscordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WikiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RedditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AutorunPatchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChangeGameFolderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CPCGAMEasdasdasdToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents VersionsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AbouToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gameversiontxtcontext As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents patchversiontxtcontext As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiscordToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WikiToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RedditToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShowToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MinimizeToTrayToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacebookToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DonateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DiscordImage As PictureBox
    Friend WithEvents kenma9123Text As Label
    Friend WithEvents patchhookok As Label
    Friend WithEvents patchhookStatus As Label
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents gamefolderStatus As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents PatreonImage As PictureBox
    Friend WithEvents PatreonToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PaypalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BitcoinToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Button1 As Button
End Class
