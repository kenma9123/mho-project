#include "stdafx.h"

#include <fstream>
#include <iostream>
#include <Windows.h>

#include <vector>
#include <string>
#include <sstream>

#include <filesystem>

using namespace std::tr2::sys;
namespace fs = std::experimental::filesystem;
using namespace std;

typedef HANDLE(__stdcall *fnOpenArchive)(const char *, uint32_t);
fnOpenArchive ifsOpenArchive;

typedef void(__stdcall * fnCreateArchive)();
fnCreateArchive ifsCreateArchive;

typedef void(__stdcall * fnFlushArchive)();
fnFlushArchive ifsFlushArchive;

typedef bool(__stdcall * fnOpenFile)(HANDLE archive, const char *filename, uint32_t, HANDLE * file, uint32_t);
fnOpenFile ifsOpenFile;

typedef void(__stdcall * fnReadFile)(uint32_t length, uint32_t *pdwRead, int unk);
fnReadFile ifsReadFile;

typedef void(__stdcall * fnAddFile)();
fnAddFile ifsAddFile;

typedef void(__stdcall *fnCloseFile)();
fnCloseFile ifsCloseFile;

void createArchive(const char *pName, uint32_t flags, uint32_t max_file_count, HANDLE *archive) {
	__asm {
		mov eax, flags
			push eax
			mov ebx, max_file_count
			push ebx
			mov ecx, pName
			mov edx, archive
			call ifsCreateArchive
	}
}

void flushArchive(HANDLE archive) {
	__asm mov esi, archive
	__asm call ifsFlushArchive
}

uint32_t readFile(HANDLE file, void *buf, uint32_t buf_size)
{
	uint32_t read_length;

	__asm {
		push 1
			lea eax, [read_length]
			push eax
			mov ecx, buf_size
			push ecx
			mov edx, buf
			mov ecx, file
			call ifsReadFile
	}

	return read_length;
}

void addFile(HANDLE archive, const char *filename, const char *storename) {
	__asm {
		mov eax, archive
			push eax
			mov edx, filename
			mov ecx, storename
			mov esi, archive
			call ifsAddFile
	}
}

void closeFile(HANDLE file) {
	__asm mov esi, file
	__asm call ifsCloseFile
}

std::vector<std::string> parseListFile(const char* list_file, uint32_t length) {
	std::stringstream list_stream(std::string(list_file, length));
	std::string file;
	std::vector<std::string> files;

	while (std::getline(list_stream, file)) {
		file.pop_back(); // remove trailing \r
		files.push_back(file);
	}

	std::cout << "done" << std::endl;

	return files;
}

std::wstring s2ws(const std::string& s) {
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

int StringToWString(std::wstring &ws, const std::string &s) {
	std::wstring wsTmp(s.begin(), s.end());

	ws = wsTmp;

	return 0;
}

bool replace(std::string& str, const std::string& from, const std::string& to) {
	size_t start_pos = str.find(from);
	if (start_pos == std::string::npos)
		return false;
	str.replace(start_pos, from.length(), to);
	return true;
}

vector<wstring> get_all_files_names_within_folder(std::string folder) {
	vector<wstring> names;
	char search_path[200];
	sprintf(search_path, "%s/*.*", folder.c_str());
	auto filepath = s2ws(search_path);
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(filepath.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				names.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	return names;
}

std::string wtoa(const std::wstring& wide) {
	std::string str;
	for (std::wstring::const_iterator it = wide.begin(); it != wide.end(); ++it) {
		str.push_back(static_cast<char>(*it));
	}
	return str;
}

bool isDir(const char *pFilename) {
	return strrchr(pFilename, '.') == nullptr;
}

void extractFiles(std::string archive_name, std::string targetDir) {
	try
	{
		std::cout << "Opening Archive .." << std::endl;
		uint32_t buf_size = 1024 * 1024 * 128; // enough?
		auto buf = new char[buf_size];

		HANDLE archive = ifsOpenArchive(archive_name.c_str(), 0);

		HANDLE list_file;
		ifsOpenFile(archive, "(listfile)", 0, &list_file, 0);

		auto length = readFile(list_file, buf, buf_size); // loop?
		closeFile(list_file);

		std::cout << "Parse list_file .." << std::endl;
		auto files = parseListFile(buf, length);

		// default export directory
		std::string export_dir = targetDir;
		fs::create_directory(export_dir);

		// extract all files and build up directory tree
		for (auto file : files) {
			// only extract common folder - which contains text
			// remove this if you want to extract all assets
			//if (file.find("common\\staticdata") == std::string::npos) {
			//	std::cout << "skipping file (" << file << ")" << std::endl;
			//	continue;
			//}

			// if directory, create thefiles inside
			if (isDir(file.c_str())) {
				std::cout << "dir " << file << std::endl;
				fs::create_directory(export_dir + "\\" + file);
			} else {
				try
				{
					if (file.find("\\.") == std::string::npos) {
						HANDLE file_handle;
						ifsOpenFile(archive, file.c_str(), 1, &file_handle, 0);

						uint32_t length = readFile(file_handle, buf, buf_size); // loop!?
						std::cout << "file (" << file << "): size: " << length << std::endl;

						// if you face this error, you have to call readFile multiple times, see the loop annotation
						if (length > buf_size)
							std::cout << "warning: need to read more! file too long (atm)!" << std::endl;

						closeFile(file_handle);
						auto out_file_path = export_dir + "\\" + file;
						if (fs::exists(out_file_path)) {
							fs::remove(out_file_path);
						}

						auto out_file = fopen(out_file_path.c_str(), "wb");
						if (out_file != nullptr) {
							fwrite(buf, 1, length, out_file);
							fclose(out_file);
						}
					}
					else {
						// TODO improve this, so it can read .extensiononly
						std::cout << "Warning: Trying to read no file name (" << file << ")" << std::endl;
					}

				}
				catch (const std::exception& e)
				{
					std::cout << "Error 1 (" << e.what() << ") " << std::endl;
				}
			}
		}

		delete[] buf;

	}
	catch (const std::exception& e)
	{
		std::cout << "Error 0 (" << e.what() << ") " << std::endl;
	}
}

bool createPatch() {
	std::string patchname = "patch.ifs";
	std::string patchfolder = "patch";

	// NOTE
	// add files here, be aware of specifing the correct path!
	// Create ifs archive
	HANDLE archive;
	
	// remove existing patch
	if (fs::exists(patchname)) {
		fs::remove(patchname);
	}

	// create the archieve
	// if you need more files increase the number (256)
	createArchive(patchname.c_str(), 0x1000000, 256, &archive);

	// get the folder tree from patch folder
	// and inject every file to the archieve
	// using the same file path
	for (recursive_directory_iterator i(patchfolder), end; i != end; ++i) {
		if (!is_directory(i->path())) {
			std::string filepath = i->path().string();

			// get the source file
			std::string source = filepath;

			// set destination path inside the archieve
			auto destination = filepath.substr(patchfolder.length() + 1);

			// all text string are stored inside common/staticdata
			addFile(archive, source.c_str(), destination.c_str());
			//std::cout << source << " source patched!" << std::endl;
			std::cout << destination << " patched!" << std::endl;
		}
	}

	// flush/close
	flushArchive(archive);

	return true;

	// addFile(archive, "currencydata.dat", "common\\staticdata\\currencydata.dat");
	// Example: addFile(archive, "my_patched_file.dat", "common\\staticdata\\my_patched_file_stored.dat");
}

void listfolder(string mainfolder) {
	for (recursive_directory_iterator i(mainfolder), end; i != end; ++i)
		if (!is_directory(i->path()))
			std::cout << i->path() << "\n" << std::endl;
}

void listData(string mainfolder = "") {
	if (mainfolder == "") {
		std::cout << "error: target folder is missing!" << std::endl;
		return;
	}

	auto files = get_all_files_names_within_folder(mainfolder);
	std::ofstream outfile;
	outfile.open("data.txt", std::ios_base::app | std::ios_base::out);
	for (auto file : files) {
		string filepath = wtoa(file);
		std::cout << filepath << std::endl;
		outfile << filepath << "\n";
	}
}

auto main(int argc, char **argv) -> int {
	
	// for extract and creating of patch

	if (strcmp(argv[1], "--extract") == 0 || strcmp(argv[1], "--create") == 0) {
		// HELP:
		// CLI: main.exe --extract/--create archive.ifs
		// warning: extract will overwrite exisiting files
		// out dir by default will be "raw/"

		// (patched IFS2.dll needed due to hashed encryption of the HET and BET table)
		if (!fs::exists("IFS2.dll")) {
			std::cout << "error: ifs2.dll is missing!" << std::endl;
			return 0;
		}

		std::wstring dll = s2ws("IFS2.dll");
		LoadLibrary(dll.c_str());
		HMODULE module = GetModuleHandle(dll.c_str());
		{
			ifsOpenArchive = (fnOpenArchive)((uint32_t)module + (0x16370));
			ifsReadFile = (fnReadFile)((uint32_t)module + (0x220C0));
			ifsOpenFile = (fnOpenFile)((uint32_t)module + (0x1FA20));
			ifsCloseFile = (fnCloseFile)((uint32_t)module + (0x20BF0));
			ifsAddFile = (fnAddFile)((uint32_t)module + (0x4A60));
			ifsCreateArchive = (fnCreateArchive)((uint32_t)module + (0xFCC0));
			ifsFlushArchive = (fnFlushArchive)((uint32_t)module + (0x17DF0));
		}

		if (strcmp(argv[1], "--create") == 0) {
			createPatch();
		}
		else if (strcmp(argv[1], "--extract") == 0) {
			std::string targetDir = "raw";
			if (argv[3] != nullptr) {
				targetDir = argv[3];
			}
			extractFiles(argv[2], targetDir);
		}
	}
	else if (strcmp(argv[1], "--listfolder") == 0) {
		listfolder(argv[2]);
	}
	else if (strcmp(argv[1], "--listdata") == 0) {
		listData(argv[2]);
	}
	else {
		std::cout << "Usage: mho-patcher [--options]" << std::endl;
		std::cout << "Options:" << std::endl;
		std::cout << "  --extract [ifs filename] - extract ifs content to raw folder" << std::endl;
		std::cout << "  --create - create a patch file patch.ifs" << std::endl;
		std::cout << "  --listdata - create reference file data.txt" << std::endl;
	}
}