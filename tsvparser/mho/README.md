# Monster Hunter Online TSV/IFS parser
--------------------------------

### Command usage:

`node mho [--options] [arguments]`

## IFS

#### version
```bash
# Get the current version of the patch
$ node mho --version
```

#### createpatch
```bash
# Create a patch, must follow the right folder tree of the files
$ node mho --createpatch
```

#### extractifs
```bash
# Extract the IFS file content to a folder
# `patchname` should be copied inside `list` folder
# `target_dir` is optional, defaults to `extracted`
$ node mho --extractifs [patchname] [target_dir]
```


## TSV

#### tsvextractall
```bash
# extract the text content of a all the tsv to the target directory
# the files must should already decrypted
$ node mho --tsvextractall [source dir] [target dir]
```

#### tsvimportall
```bash
# import a multiple text content to multiple tsvfiles
# output file is a decrypted form of a tsv
$ node mho --tsvimportall [source dir] [target dir] [reference dir]
```

#### tsvimportencall
```bash
# like `tsvimportall` command but the final step is to encrypt everything
$ node mho --tsvimportencall [source csv] [target dat] [target enc]
```

#### tsvmerge
```bash
# merge old and new versions of tsv - via its text content
$ node mho --tsvmerge [tsvname]
```


## CSV

#### csvmerge
```bash
# merge a downloaded csv to extracted csv
$ node mho --csvmerge [target_merge] [source_mergedir] [output_dir]
```


## ENC/DEC

#### encall
```bash
# encrypt all tsv files inside a directory - the files must be decrypted
$ node mho --encall [source_dir] [target_dir]
```

#### decall
```bash
# decrypt all tsv files inside a directory - the files must be encrypted
$ node mho --decall [source_dir] [target_dir]
```

#### checkenc
```bash
# Check if encyption is still supported
# `target_dir` must point to the extracted IFS content
$ node mho --checkenc [target_dir]
```

## UI

#### testffdec
```bash
# Test and check ffdec library
$ node mho --testffdec
```

#### swf2xml
```bash
# Extract the xml of a swf
# make sure every swf to extract is inside
# `uiswf` folder from root or from specified source
# this extract the xml files on the same folder
$ node mho --swf2xml [target dir]
```

#### xml2swf
```bash
# Import the xml to swf
$ node mho --xml2swf [sourcedir] [target dir]
```

#### swfxml2csv
```bash
# extract the csv version of swfxml
# `sourceDir` where to get the swfxml from
# `targetDir` where to save the extracted csv
$ node mho --swfxml2csv [swfxml sourceDir] [csv targetDir]
```

#### csv2swfxml
```bash
# NOTE: for this function to work properly
# the xml2js library should be modified to wrap the
# rdf tag inside CDATA
# if (!!~child.indexOf('<rdf:RDF')) {
#   element = element.raw(wrapCDATA(child));
# }
# import the csv to swfxml
# `sourceDir` where to get the csv from
# `targetDir` where to save the modified xml
# `referenceDir` where to get the original xml files
$ node mho --csv2swfxml [csv sourceDir] [swfxml targetDir] [reference Dir]
```

#### uixml2csv
```bash
# Extract the csv of the ui xml
# these xml are the files under the `xml` folder
# `sourceDir` where to get the uixml from
# `targetDir` where to save the extracted csv
$ node mho --uixml2csv [uixml sourceDir] [csv targetDir]
```

#### csv2uixml
```bash
# import the csv to uixml
# `sourceDir` where to get the csv from
# `targetDir` where to save the modified xml
# `referenceDir` where to get the original xml files
$ node mho --csv2uixml [csv sourceDir] [uixml targetDir] [reference Dir]
```

## DOWNLOAD

#### uploadallcsv
```bash
# Upload all csv to gsheets
$ node mho --uploadallcsv [source dir] [gfolder_id]
```

#### updateallcsv
```bash
# Update all csv to gsheets
# if the source dir has files that are not present
# from the gfolder, it will automatically be upload
$ node mho --updateallcsv [source dir] [gfolderid]
```

#### downloadcsvfolder
```bash
# download all csv files from google spreadsheets / gdrive folder to a target folder
$ node mho --downloadcsvfolder [target folder] [gfolderid]
```

## UTILS

#### filterenc
```bash
# Remove .dat files that aren't included inside `tsv.json`
$ node mho --filterenc
```

#### recopyenc
```bash
# Copy the enc files from source folder
# to target folder `staticdata_enc`
# this will only copy the files that are present
# to the target folder
$ node mho --recopyenc [source dir] [target dir]
```

#### copyfile
```bash
# Copy a single file to directory
$ node mho --copyfile [source file] [target file]
```

#### copyfiles
```bash
# Copy files from source to target directory
$ node mho --copyfiles [source dir] [target dir]
```

## Author
kenma9123
