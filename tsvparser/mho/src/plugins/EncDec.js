import fs from 'fs';
import path from 'path';
import Common from '../Common';
import Crypto from '../Crypto';

class EncDec extends Common {

  /**
   * encrypt all tsv files inside a directory - the files must be decrypted
   * Usage
   * node mho --encall [source dir] [target dir]
   */
  encall() {
    this._cryptall('encall', 'encrypt');
  }

  /**
   * decrypt all tsv files inside a directory - the files must be encrypted
   * Usage
   * node mho --decall [source dir] [target dir]
   */
  decall() {
    this._cryptall('decall', 'decrypt');
  }

  /**
   * Check if encyption is still supported
   * `target_dir` must point to the extracted IFS content
   * node mho --checkenc [extracted]
   */
  checkenc() {
    console.time('checkenc');

    const sourceFolder = (process.argv[3] !== undefined) ? process.argv[3] : 'extracted';
    const cwd = 'ifs';

    const extractedFolder = path.join(cwd, sourceFolder);

    // check source dir
    if (!fs.existsSync(extractedFolder)){
      this.die(`Can't find the source folder \`${sourceFolder}\` inside \`${cwd}\` folder`);
    }

    // lets find the common/staticdata folder
    // if its exist and has .dat files
    const staticFolder = path.join(extractedFolder, 'common', 'staticdata');
    // console.log(staticFolder);
    if (!fs.existsSync(staticFolder)) {
      this.die(`Staticdata folder is missing, try to extract another IFS file`);
    }

    let crypto = new Crypto();
    const files = fs.readdirSync(staticFolder);
    let hasDatFiles = false;
    if (files && files.length > 0) {
      for (var x = 0; x < files.length; x++) {
        let file = files[x];
        // console.log(file);

        // if we found a dat lets decode it
        if (~file.indexOf('.dat')) {
          hasDatFiles = true;

          // directly decrypt the first files
          // console.log(path.join(staticFolder, file));
          crypto.directDecrypt(path.join(staticFolder, file)).then(buffer => {
            let headerSouce = Buffer.from('#TSV');
            let headerTarget = Buffer.alloc(headerSouce.length);
            buffer.copy(headerTarget, 0, 0, headerTarget.length);
            if (headerTarget.equals(headerSouce)) {
              // console.log(headerTarget.toString());
              // console.log(headerSouce.toString());
              console.log('Encryption key supported.');
            } else {
              console.log('Unsupported key encryption.');
            }
          });
          break;
        }
      }

      if (!hasDatFiles) {
        this.die('No .dat files found, try to extract another IFS file.');
      }
    }
  }

  _cryptall(name, method, optArgs = false) {
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    // create output dir if not exist
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    let crypto = new Crypto();

    // read all files
    console.time(name);
    const files = fs.readdirSync(sourceDir);
    this.loop(files, (file, next) => {
      const sourceFile = `${sourceDir}/${file}`;
      const targetFile = `${targetDir}/${file}`;
      crypto[method](sourceFile, targetFile, next);
    }, () => {
      console.timeEnd(name);
    }, () => {
      this.die(`Could not list the directory: ${sourceDir}`);
    });
  }
}

export default EncDec;
