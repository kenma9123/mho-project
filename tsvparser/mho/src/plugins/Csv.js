import fs from 'fs';
import path from 'path';
import Common from '../Common';
import CsvMerger from '../CsvMerger';

class Csv extends Common {

  /**
   * Merge downloaded csv to the extracted csv
   * Usage:
   * node mho --csvmerge [targetMerge] [sourceMerge dir]  [output dir]
   * Sample:
   * node mho --csvmerge staticdata_csv downloaded_translation staticdata_csv_merge
   */
  csvmerge() {
    const targetMergeDir = process.argv[3];
    const sourceMergeDir = process.argv[4];
    const outputDir = process.argv[5];

    if (targetMergeDir === undefined || !fs.existsSync(targetMergeDir)) {
      this.die('Target merge directory is missing');
    }

    if (sourceMergeDir === undefined || !fs.existsSync(sourceMergeDir)) {
      this.die('Source merge directory is missing');
    }

    if (outputDir === undefined) {
      this.die('Outpur directory is missing');
    }

    // create output dir if not exist
    if (!fs.existsSync(outputDir)){
      fs.mkdirSync(outputDir);
    }

    const csvMerger = new CsvMerger();

    // read target dir for ref merge csv
    console.time('csvmerge');
    const dfiles = fs.readdirSync(targetMergeDir);
    this.loop(dfiles, (file, next) => {
      let targetMergeFile = `${targetMergeDir}/${file}`;
      let sourceMergeFile = `${sourceMergeDir}/${file}`;
      let outputFile = `${outputDir}/${file}`;

      // make only csv files
      if (path.extname(file) !== '.csv') {
        next();
      } else {
        // make sure that the equivalent files from sourceDir
        // is also existed inside the targetDir
        // if not, just copy the targe file to output file directly
        if (!fs.existsSync(sourceMergeFile)) {
          console.log(`Source file not found, copying ${file} to output folder.`);
          this.copyFile(targetMergeFile, outputFile, next);
        } else {
          // console.log('Mergin file to', targetMergeFile);
          // console.log('Mergin file from', sourceMergeFile);
          // console.log('Output file to', outputFile);
          csvMerger.merge(targetMergeFile, sourceMergeFile, outputFile, next);
        }
      }
    }, () => {
      console.timeEnd('csvmerge');
    }, () => {
      console.log('Downloaded csv files are missing');
    });
  }
}

export default Csv;
