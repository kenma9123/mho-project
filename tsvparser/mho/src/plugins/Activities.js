import fs from 'fs';
import path from 'path';
import Common from '../Common';
import ActivityHandler from '../ActivityHandler';

class Activities extends Common {

  /**
   * Extract text from activity files
   * they reside on lua files
   */
  lua2csv() {
    const Activity = new ActivityHandler();
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    Activity.lua2csv(sourceDir, targetDir);
  }

  /**
   * Import text to lua files
   */
  csv2lua() {
    const Activity = new ActivityHandler();
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];
    const referenceDir = process.argv[5];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    if (referenceDir === undefined) {
      this.die('Reference directory is required');
    }

    Activity.csv2lua(sourceDir, targetDir, referenceDir);
  }
}

export default Activities;
