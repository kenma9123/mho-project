import fs from 'fs';
import xml2js from 'xml2js';
import path from 'path';

class Common {

  static getTsvStartTag() {
    return '#============Begin of sheet============';
  }

  static getTsvEndTag() {
    return '#============End of sheet============';
  }

  constructor() {
    this.tsvref = {};
  }

  setTsvRef(ref) {
    this.tsvref = ref;
  }

  die() {
    console.log.apply(null, arguments)
    process.exit(1);
  }

  loop(array = [], cb = false, done = false, error = false) {
    if (array.length > 0) {
      const iterator = (i = 0) => {
        if (i >= array.length) {
          done && done();
          return;
        }

        cb && cb(array[i], () => {
          iterator(++i);
        }, i);
      };

      // start iteration
      iterator();
    } else {
      error && error();
    }
  }

  copyFile(source, target, cb) {
    var cbCalled = false;

    var rd = fs.createReadStream(source);
    rd.on("error", function(err) {
      done(err);
    });
    var wr = fs.createWriteStream(target);
    wr.on("error", function(err) {
      done(err);
    });
    wr.on("close", function(ex) {
      done();
    });
    rd.pipe(wr);

    function done(err) {
      if (!cbCalled) {
        cb(err);
        cbCalled = true;
      }
    }
  }

  renameFile(source, target, cb) {
    fs.rename(source, target, function(err) {
      if ( err ) console.log('ERROR: ' + err);
      cb && cb();
    });
  }

  getLinedataOfEachSheets(tsvpath, cb = false) {
    // read a tsv file
    const content = fs.readFileSync(tsvpath, 'utf-8');
    if (content) {
      // console.log("Reading TSV", content);
      // http://stackoverflow.com/questions/14969810/extract-text-between-paragraph-tag-using-regex
      const startTag = Common.getTsvStartTag();
      const endTag = Common.getTsvEndTag();
      let regexp = new RegExp(`${startTag}\\r?\\n?([\\s\\S]*?)\\r?\\n?${endTag}`, 'g');
      let matches;

      let sheetIndex = 1;
      while (matches = regexp.exec(content)) {
        let lines = matches[1].split(/\r?\n/g);
        cb && cb(lines);
      }
    }
  }

  parseTSVXmlAndExtract(tsvpath, cb = false) {
    // read a tsv file
    let parser = new xml2js.Parser();
    const content = fs.readFileSync(tsvpath, 'utf-8');
    if (content) {
      parser.parseString(content, (err, result) => {
        if (err) {
          cb && cb(false);
        } else {
          // extract the text from location
          let { Objs: {Obj} } = result;
          if (Obj) {
            cb && cb(Obj);
          } else {
            cb && cb(false);
          }
        }
      });
    }
  }

  parseXmlStringAndExtract(content, cb = false) {
    // read a tsv file
    let parser = new xml2js.Parser();
    if (content) {
      parser.parseString(content, (err, result) => {
        if (err) {
          cb && cb(false);
        } else {
          cb && cb(result.Obj);
        }
      });
    }
  }

  getJSONEquivalent(source, valueIndex = 1) {
    let json = {};
    let lines = source.split(/\r?\n/g);
    for (let x = 0; x < lines.length; x++) {
      if (x !== 0) {
        let linedata = lines[x].split(this.getTabDelimiter());
        if (linedata[0]) {
          json[linedata[0]] = linedata[valueIndex];
        }
      }
    }

    return json;
  }

  getQuestJSONEquivalent(source) {
    let json = {};
    let lines = source.split(/\r?\n/g);
    for (let x = 0; x < lines.length; x++) {
      if (x !== 0) {
        let linedata = lines[x].split(this.getTabDelimiter());
        let id = linedata[0].replace('(NOEDIT)', '');
        if (!(id in json)) {
          json[id] = [];
        }

        json[id] = linedata.splice(1);
      }
    }

    return json;
  }

  humanFilesize(size, precision = 2) {
    let i = 0;
    for (i = 0; (size / 1024) > 0.9; i++, size /= 1024) {}
    return Math.round(size, precision) + ['bytes','KB','MB','GB','TB','PB','EB','ZB','YB'][i];
  }

  getTSVReference() {
    return new Promise((resolve, reject) => {
      // get tsv content and find the refernce for
      // the current tsv name
      this.getTSVReferenceContent().then((tsvjson) => {
        tsvjson.map((tsv) => {
          if (tsv.name === this.props.tsvname && !('disabled' in tsv)) {
            resolve(tsv);
          }
        });
      }).catch(reject);
    });
  }

  getTSVRefSheetsOptions(sheetname) {
    let options = {};
    for (let x in this.tsvref.sheets) {
      let sheet = this.tsvref.sheets[x];
      if (!('disabled' in sheet) && sheet.name === sheetname) {
        options = sheet;
        break;
      }
    }
    return options;
  }

  isTsvRefHasAnXML() {
    return ('xml' in this.tsvref && this.tsvref.xml === true);
  }

  getTSVRefExtractFromTags(sheetname) {
    let sheets = this.tsvref.sheets;
    let tags = [];
    for( let x in sheets) {
      let sheet = sheets[x];
      if (!('disabled' in sheet) &&
        sheet.name === sheetname &&
        ('withTags' in sheet && sheet.withTags)) {
        tags = sheet.extractFromTags;
        break;
      }
    }
    return tags;
  }

  isRefSheetUniqueSheet() {
    return ('uniqueSheet' in this.tsvref && this.tsvref.uniqueSheet) ? true : false;
  }

  getTSVReferenceContent() {
    return new Promise((resolve, reject) => {
      this.isFileExist('tsv.json').then(() => {
        // Read the file.
        fs.readFile('tsv.json', 'utf-8', (error, data) => {
          resolve(JSON.parse(data));
        });
      }).catch(reject);
    });
  }

  getUIReferenceContent() {
    return new Promise((resolve, reject) => {
      this.isFileExist('ui.json').then(() => {
        // Read the file.
        fs.readFile('ui.json', 'utf-8', (error, data) => {
          let uijson = JSON.parse(data).filter(ui => !('xml' in ui) && !('disabled' in ui));
          resolve(uijson);
        });
      }).catch(reject);
    });
  }

  getAllUIReferenceContent(sourceDir) {
    return new Promise((resolve, reject) => {
      let files = fs.readdirSync(sourceDir);
      if (!files) {
        reject();
      } else {
        files = files.filter(file => path.extname(file) === '.swf').map(file => {
          return {
            name: path.basename(file, path.extname(file))
          };
        });
        resolve(files);
      }
    });
  }

  getUIXMLReferenceContent() {
    return new Promise((resolve, reject) => {
      this.isFileExist('ui.json').then(() => {
        // Read the file.
        fs.readFile('ui.json', 'utf-8', (error, data) => {
          let uijson = JSON.parse(data).filter(ui => ('xml' in ui) && !('disabled' in ui));
          resolve(uijson);
        });
      }).catch(reject);
    });
  }

  getLimitReferenceContent() {
    return new Promise((resolve, reject) => {
      this.isFileExist('limits.json').then(() => {
        // Read the file.
        fs.readFile('limits.json', 'utf-8', (error, data) => {
          resolve(JSON.parse(data));
        });
      }).catch(reject);
    });
  }

  getTSVXMLReference() {
    const tsvs = fs.readFileSync('tsv.json', 'utf-8');
    return JSON.parse(tsvs).reduce((prev, tsv) => {
      if ('xml' in tsv && tsv.xml) {
        prev.push(tsv);
      }

      return prev;
    }, []);
  }

  isFileExist(filename) {
    return new Promise((resolve, reject) => {
      fs.stat(filename, (errors, stat) => {
        if (errors == null) {
          resolve();
        } else {
          reject(errors);
        }
      });
    });
  }

  getTabDelimiter(count = 1) {
    let tabs = [];
    for (let x = 0; x < count; x++) {
      tabs.push('\t');
    }
    return tabs.join('');
  }

  getNewLineDelimiter(count = 1) {
    let nl = [];
    for (let x = 0; x < count; x++) {
      nl.push('\r\n');
    }
    return nl.join('');
  }

  utf8StringToByteArray(str) {
    // TODO(user): Use native implementations if/when available
    var out = [], p = 0;
    for (var i = 0; i < str.length; i++) {
      var c = str.charCodeAt(i);
      if (c < 128) {
        out[p++] = c;
      } else if (c < 2048) {
        out[p++] = (c >> 6) | 192;
        out[p++] = (c & 63) | 128;
      } else if (
          ((c & 0xFC00) == 0xD800) && (i + 1) < str.length &&
          ((str.charCodeAt(i + 1) & 0xFC00) == 0xDC00)) {
        // Surrogate Pair
        c = 0x10000 + ((c & 0x03FF) << 10) + (str.charCodeAt(++i) & 0x03FF);
        out[p++] = (c >> 18) | 240;
        out[p++] = ((c >> 12) & 63) | 128;
        out[p++] = ((c >> 6) & 63) | 128;
        out[p++] = (c & 63) | 128;
      } else {
        out[p++] = (c >> 12) | 224;
        out[p++] = ((c >> 6) & 63) | 128;
        out[p++] = (c & 63) | 128;
      }
    }
    return out;
  }

  replaceAll(src, str1, str2, ignore, whole) {
    let expression = str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&");
    let regex = new RegExp((whole?`(\s|^)${expression}(?=\s|$)`:expression), (ignore?"gi":"g"));
    return src.replace(regex, (typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
  }

  readDirRecursive(dir) {
    return fs.statSync(dir).isDirectory()
          ? Array.prototype.concat(...fs.readdirSync(dir).map(f => this.readDirRecursive(path.join(dir, f))))
          : dir;
  }
}

export default Common;
