import fs from 'fs';
import xml2js from 'xml2js';
import chalk from 'chalk';
import Common from './Common';
import crypto from 'crypto';
import jsonfile from 'jsonfile';
import isEmpty from 'lodash/isEmpty';

class TsvImporter extends Common {

  constructor() {
    super();

    // read xml tsv xml files
    this.tsvXMLFiles = this.getTSVXMLReference().map(tsv => tsv.name);

    // we need dec dir
    // for .dat reference
    this.csvSourceContentJSON = {};
  }

  parseXML(filepath, cb = false) {
    let parser = new xml2js.Parser();
    const content = fs.readFileSync(filepath, 'utf-8');
    if (content) {
      parser.parseString(content, (err, result) => {
        cb && cb(result);
      });
    }
  }

  parseXMLContent(content, cb = false) {
    let parser = new xml2js.Parser();
    if (content) {
      parser.parseString(content, (err, result) => {
        cb && cb(result);
      });
    }
  }

  import(sourceFile, targetDir, referenceDir, cb = false) {

    // source file should be a csv
    if (!~sourceFile.indexOf('.csv')) {
      console.error('Source file must be a csv file');
      return false;
    }

    // create target dir if not exist
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    let tsvname = sourceFile.split('/')[1].split('.')[0];
    let tsvpath = `${referenceDir}/${tsvname}.dat`;
    let csvSourcepath = `${sourceFile}`;

    cb && console.time(`import ${tsvname}`);

    if (!this.isFileExist(tsvpath)) {
      console.error(`ERROR: TSV File ${tsvpath} doesn't exist.`);
      return false;
    }

    // read the csv translated file
    fs.readFile(csvSourcepath, 'utf-8', (error, csvSourceContent) => {
      this.csvSourceContentJSON = this.getJSONEquivalent(csvSourceContent, 2);

      let fcontent = '';

      // for tsv xml files, process them different
      if (!!~this.tsvXMLFiles.indexOf(tsvname)) {
        var xmlbuilder = new xml2js.Builder({
          headless: true,
          includeWhiteChars: true,
          renderOpts: {
            'pretty': true,
            'indent': '    ',
            'newline': '\n'
          }
        });

        // read dat file xml
        // console.log(tsvpath);
        const locations = ['Name', 'Note', 'Description'];
        this.parseXML(tsvpath, result => {
          result.Objs.Obj.forEach((obj, oindex) => {
            // console.log(obj);
            // find from locations
            locations.forEach(location => {
              if (location in obj) {
                let text = String(obj[location][0]);
                // console.log(text);
                if (text === 0 || text == 0 || text === '0' || text === '') {
                  // console.log(text);
                } else {
                  // create hash/signature of the text
                  const hash = crypto.createHash('md5').update(text).digest("hex");
                  // console.log(hash);

                  if (hash in this.csvSourceContentJSON) {
                    text = this.csvSourceContentJSON[hash];

                    // modify text, double, single, new lines stuff
                    text = this.replaceSpecialChars(text);

                    // replace xml
                    result
                      .Objs
                        .Obj[oindex][location][0] = text;
                  }
                }
              }
            });
          });

          var xml = xmlbuilder.buildObject(result);
          // replace some strings like white spaces on empty nodes
          fcontent = xml.replace(/\/\>/g, ' />').replace(/{DQ}/g, '&quot;').replace(/\\#/g, '#');
          // fs.writeFile(`${targetDir}/${tsvname}.dat`, xml, (err) => {
          //   if (err) throw err;
          //   if (cb) {
          //     console.timeEnd(`import ${tsvname}`);
          //     cb();
          //   } else {
          //     console.log(`${tsvname} imported`);
          //   }
          // });
        });
      } else {
        let tsvTable = [];
        this.getLinedataOfEachSheets(tsvpath, (lines) => {

          let sheetTable = [];
          let sheetName = lines[0];
          let sheetHeader = lines[1];

          // push sheet tag
          sheetTable.push(Common.getTsvStartTag());

          // push the sheetname to sheettable
          sheetTable.push(sheetName);

          // push table header to sheettable
          sheetTable.push(sheetHeader);

          // tsv reference json
          const sheetOpt = this.getTSVRefSheetsOptions(sheetName);
          const sheetXML = ('xml' in sheetOpt && !isEmpty(sheetOpt.xml)) ? sheetOpt.xml : false;
          const columns = (!isEmpty(sheetOpt.cols)) ? sheetOpt.cols : [];
          // console.log('columns', columns);

          lines.forEach((line, index) => {
            // skip sheetname and header
            if (index > 1) {
              let linedata = line.split(this.getTabDelimiter());
              let newlinedata = [...linedata];

              // read text from a column reference
              if (columns.length > 0) {
                columns.forEach((col) => {
                  let target = Number(col) - 1;
                  let text = linedata[target];

                  if (!sheetXML) {
                    // do not add empty text
                    if (text === 0 || text == 0 || text === '0' || text === '') {
                      // console.log(text);
                    } else {
                      // create hash/signature of the text
                      const hash = crypto.createHash('md5').update(text).digest("hex");

                      // modify text, double, single, new lines stuff
                      // text = this.replaceSpecialChars(text);

                      // this method automatically replace identical text
                      if (hash in this.csvSourceContentJSON) {
                        let translated = this.csvSourceContentJSON[hash];
                        // console.log(translated);
                        newlinedata[target] = this.replaceSpecialChars(translated);
                      }
                    }
                  } else {
                    var xmlbuilder = new xml2js.Builder({
                      headless: true,
                      includeWhiteChars: true,
                      emptyTag: ''
                    });

                    this.parseXMLContent(JSON.parse(text), result => {
                      sheetXML.forEach(location => {
                        if (location in result.Obj) {
                          let text = String(result.Obj[location][0]);
                          // console.log(text);
                          if (text === 0 || text == 0 || text === '0' || text === '') {
                            // console.log(text);
                          } else {
                            // create hash/signature of the text
                            const hash = crypto.createHash('md5').update(text).digest("hex");
                            // console.log(hash);

                            if (hash in this.csvSourceContentJSON) {
                              text = this.csvSourceContentJSON[hash];

                              // replace xml
                              result
                                .Obj[location][0] = text;
                            }
                          }
                        }
                      });

                      var xml = xmlbuilder.buildObject(result);
                      newlinedata[target] = JSON.stringify(xml);
                    });
                  }
                });
              }

              // push the new line to sheet table
              sheetTable.push(newlinedata.join(this.getTabDelimiter()));
            }
          });

          // push sheet tag
          sheetTable.push(Common.getTsvEndTag());

          // push everything to table
          tsvTable.push(sheetTable.join('\n'));
        });

        let fileheader = `#TSV${this.getNewLineDelimiter()}`;
        let data = tsvTable.join(this.getNewLineDelimiter(2));
        fcontent = `${fileheader}${data}${this.getNewLineDelimiter(2)}`;

        if (tsvname === 'activitydata' || tsvname === 'equiprandpassiveskilldata') {
          fcontent += `#EOF${this.getNewLineDelimiter()}`;
        }
      }

      // convert everything to buffer
      let fcontentByteArray = this.utf8StringToByteArray(fcontent);
      let fcontentBuffer = new Buffer(fcontentByteArray);

      // add padding for encryption
      let remainder = fcontentByteArray.length % 16;
      let extraBytes = 16 - remainder;

      // add more 16bytes tail for tsv xml files
      if (!!~this.tsvXMLFiles.indexOf(tsvname)) {
        extraBytes += 16;
      }

      let totalbufflen = fcontentByteArray.length + extraBytes;
      const finalbuff = Buffer.concat([fcontentBuffer, Buffer.alloc(extraBytes)], totalbufflen);

      fs.writeFile(`${targetDir}/${tsvname}.dat`, finalbuff, 'binary', (err) => {
        if (err) throw err;
        if (cb) {
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(`import ${tsvname}`);
          cb();
        } else {
          console.log(`${tsvname} imported`);
        }
      });
    });
  }

  replaceSpecialChars(text) {
    return text
      .replace(/{DQ}/g, '\"')
      .replace(/{NL}/g, '\\n')
      .replace(/{TAB}/g, '\t')
      .replace(/{SPC}/g, ' ');
  }
}

export default TsvImporter;
