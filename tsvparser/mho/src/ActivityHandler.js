import fs from 'fs';
import path from 'path';
import Common from './Common';
import crypto from 'crypto';
import isUndefined from 'lodash/isUndefined';
import Iconv from 'iconv-lite';
import fx from 'mkdir-recursive';
import luaparse from 'luaparse';
import jsonfile from 'jsonfile';
import moment from 'moment';

class ActivityHandler extends Common {

  constructor() {
    super();

    this.refNames = [
      'ACTIVITY_NAME',
      'ACTIVITY_TIME',
      'ACTIVITY_RULE',
      // 'SHOW_DATE',
      // 'START_DATE',
      'STOP_DATE',
      'ACTIVITY_DATA'
    ];

    this.refDates = [
      'SHOW_DATE',
      'START_DATE',
      'STOP_DATE'
    ];

    this.activityDataRefNames = [
      'szDesc',
      'szLabel'
    ];
  }

  lua2csv(sourceDir, targetDir) {

    // make sure source dir is present
    if (!fs.existsSync(sourceDir)) {
      console.log(`Source directory \`${sourceDir}\` is missing`);
      process.exit(1);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    // only read lua files from the activities.txt
    let activitiesPath = path.join(sourceDir, 'activities.txt');
    if (!fs.existsSync(activitiesPath)) {
      this.die(`Activities.txt is missing from ${sourceDir}`);
    }

    let activities = fs.readFileSync(activitiesPath, 'utf-8');
    let luafiles = activities.split(/\r\n/gm).filter(file => path.extname(path.basename(file)) === '.lua').map(file => path.join(sourceDir, file));
    let hashTable = {};
    // console.log(luafiles.length);
    let count = 0;
    this.loop(luafiles, (luafile, tnext) => {

      // proceed to next if not exist
      if (!fs.existsSync(luafile)) {
        console.log(`File does not exist ${luafile}`);
        return tnext();
      }

      let buffer = fs.readFileSync(luafile);
      if (!buffer) {
        console.log('Unable to extract', luafile);
        return tnext();
      }

      let luacodesource = Iconv.decode(buffer, 'CP936');
      let lua = luaparse.parse(luacodesource, {comments: false});

      // for debugging
      // jsonfile.writeFile(path.join(targetDir, 'file.json'), lua, {spaces: 2}, function(err) {
      //   console.error(err)
      // });

      if ('body' in lua) {

        const textToHashTable = (value) => {
          let hasChinese = String(value).match(/[\u4e00-\u9fa5]/);
          if (hasChinese) {
            // create hash/signature of the text
            const hash = crypto.createHash('md5').update(value).digest("hex");
            value = value
              .replace(/\r\n/g, '{NL}').replace(/\r/g, '{NLD}')
              .replace(/\n/g, '{NLN}').replace(/\t/g, '{TAB}')
              .replace(/\s/g, '{SPC}').replace(/“/g, '{DQ}');
            hashTable[hash] = value;
          }
        };

        let isActiveEvent = true;
        let luaStopDate = lua.body
          .filter(expr => expr.type === 'AssignmentStatement')
          .find(expr => expr.variables[0].name === 'STOP_DATE');

        // only get active events
        // if no STOP_DATE means its still active
        // if theres STOP_DATE check if the date is active
        if (luaStopDate) {
          // make sure stop date
          let { type, fields } = luaStopDate.init[0];
          if (type === 'TableConstructorExpression') {
            let [ year, month, date, hour, minutes, seconds ] = fields;
            let stopDate = moment().year(year.value.value).month(month.value.value - 1).date(date.value.value);
            // console.log('STOP DATE IS ', stopDate.format('YYYY MM DD'), stopDate > moment());
            isActiveEvent = stopDate > moment();
          }
        }

        // if not an active event
        // don't include it
        if (!isActiveEvent) {
          console.log(luafile, 'is inactive');
          return process.nextTick(tnext);
        }

        console.log(luafile, 'is still active');
        // console.log('Extracting', luafile, ++count);

        lua.body
          .filter(expr => expr.type === 'AssignmentStatement')
          .filter(expr => this.refNames.includes(expr.variables[0].name)) // only include ref names
          .filter(expr => !this.refDates.includes(expr.variables[0].name)) // remove ref dates
          .forEach(expr => {
            // console.log('final', expr);

            let { value = '', left = {}, right = {}, fields = [], type } = expr.init[0];

            switch (type) {
              case 'StringLiteral':
                textToHashTable(value);
              break;

              // mostly ACTIVITY_RULE
              case 'BinaryExpression':
                textToHashTable(left.value);
                textToHashTable(right.value);
              break;

              case 'TableConstructorExpression':
                fields.forEach(field => {
                  field.value.fields
                    .filter(expr => expr.type === 'TableKeyString' && this.activityDataRefNames.includes(expr.key.name))
                    .forEach(vfield => {
                      textToHashTable(vfield.value.value);
                    });
                });
              break;
            }
          });
      }

      process.nextTick(tnext);
    }, () => {
      // console.log(hashTable);
      let fcontent = 'ID_NO_EDIT\tORIGINAL_NO_EDIT\tTRANSLATE_EDIT\n';
      Object.keys(hashTable).forEach((hash) => {
        fcontent += `${hash}\t${hashTable[hash]}\t${hashTable[hash]}\n`;
      });

      let targetFile = path.join(targetDir, 'activities.csv');
      fs.writeFile(targetFile, fcontent, (err) => {
        if (err) throw err;
      });
      console.log('All lua has been extracted to csv');
    }, () => {
      console.log('No files to extract');
    });
  }

  csv2lua(sourceDir, targetDir, referenceDir) {
    // make sure source dir is present
    if (!fs.existsSync(sourceDir)) {
      console.log(`Source directory \`${sourceDir}\` is missing`);
      process.exit(1);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    // only read lua files from the activities.txt
    let activitiesPath = path.join(referenceDir, 'activities.txt');
    if (!fs.existsSync(activitiesPath)) {
      this.die(`Activities.txt is missing from ${referenceDir}`);
    }

    // read the source file for translation
    let sourcetranslationFile = path.join(sourceDir, `activities.csv`);
    if (!fs.existsSync(sourcetranslationFile)) {
      this.die(`Source translation missing on ${sourcetranslationFile}`);
    }

    fs.readFile(sourcetranslationFile, 'utf-8', (error, sourceTrans) => {
      let csvSourceContentOriginalJSON = this.getJSONEquivalent(sourceTrans, 1);
      let csvSourceContentTranslatedJSON = this.getJSONEquivalent(sourceTrans, 2);

      let activities = fs.readFileSync(activitiesPath, 'utf-8');
      let luafiles = activities.split(/\r?\n/g).filter(file => path.extname(path.basename(file)) === '.lua');
      this.loop(luafiles, (luafilebase, tnext) => {
        let luafile = path.join(referenceDir, luafilebase);

        // proceed to next if not exist
        if (!fs.existsSync(luafile)) {
          console.log(`File does not exist ${luafile}`);
          return tnext();
        }

        let buffer = fs.readFileSync(luafile);
        if (!buffer) {
          console.log('Unable to import', luafile);
          return tnext();
        }

        let luacodesource = Iconv.decode(buffer, 'CP936');
        let lines = luacodesource.split(/\r?\n/g);

        // replace every data of target from source
        let isDirty = false;

        // compare every line of string to the source file
        // read source file
        for (let x = 0; x < lines.length; x++) {
          let sourceLine = lines[x];
          let hasChinese = String(sourceLine).match(/[\u4e00-\u9fa5]/);
          if (hasChinese) {
            for (let hash in csvSourceContentOriginalJSON) {
              let originalText = csvSourceContentOriginalJSON[hash]
                .replace(/{NL}/g, '\r\n').replace(/{NLD}/g, '\r')
                .replace(/{NLN}/g, '\n').replace(/{TAB}/g, '\t')
                .replace(/{SPC}/g, ' ').replace(/{DQ}/g, '“');

              let translatedText = csvSourceContentTranslatedJSON[hash]
                .replace(/{NL}/g, '\r\n').replace(/{NLD}/g, '\r')
                .replace(/{NLN}/g, '\n').replace(/{TAB}/g, '\t')
                .replace(/{SPC}/g, ' ').replace(/{DQ}/g, '“');

              // console.log('original, translate', originalText, translatedText);
              // console.log('sourceline', sourceLine);
              // console.log('originalText', originalText);
              // console.log('source line', sourceLine);
              let replaced = this.replaceAll(sourceLine, JSON.stringify(originalText), JSON.stringify(translatedText));

              if (replaced !== sourceLine) {
                // console.log('replaced', originalText, 'with', translatedText);
                lines[x] = replaced;
                isDirty = true;
              }
            }
          }
        }

        if (isDirty) {
          // console.log('Importing lua file', luafile);
          let timename = `csv2lua ${luafile}`;
          console.time(timename);
          // console.log(`modifying ${filepath}`);
          // \r\n is essential so ffdec can detect if formatted or not
          // it happens from ffdec TextImporter.java
          let dirname = path.dirname(luafilebase);
          let targetDirName = path.join(targetDir, dirname);
          if (!fs.existsSync(targetDirName)) {
            // console.log('Not exist dir', targetDirName);
            fx.mkdirSync(targetDirName.toLowerCase());
          }

          let finalbuf = Iconv.encode(lines.join('\r\n'), 'CP936');
          fs.writeFile(path.join(targetDir, luafilebase.toLowerCase()), finalbuf, (err) => {
            if (err) throw err;
            console.timeEnd(timename);
            tnext();
          });
        } else {
          tnext();
        }
      }, () => {
        console.log('All csv has been imported to lua');
      }, () => {
        console.log('No files to import');
      });
    });
  }
}

export default ActivityHandler;
