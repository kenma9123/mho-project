import fs from 'fs';
import Common from './Common';
import crypto from 'crypto';
import each from 'async/each';

class TsvMerger extends Common {

  constructor() {
    super();

    this.questFiles = [
      ['quest_doris', 'Sheet_1'],
      ['quest_duck', 'Sheet_1'],
      ['quest_ran', 'Sheet_1'],
      ['quest_stephen', 'Sheet_1'],
      ['quest_yi', 'Sheet_1'],
      ['questgroup', 'Sheet_2'],
      ['questlib', 'Sheet_3'],
      ['questchapter', 'Sheet_4'],
      ['questseries', 'Sheet_5']
    ];

    this.csvMergeDir = `staticdata_csv_merge/`;
    // create dir if not exist
    if (!fs.existsSync(this.csvMergeDir)){
      fs.mkdirSync(this.csvMergeDir);
    }
  }

  merge(tsvname, cb = false) {

    this.tsvname = tsvname;
    this.csvSourceContentJSON = {};

    const csvSourcepath = `staticdata_csv/${this.tsvname}.csv`;

    // read the csv source first on where to merge the translated files
    console.time(this.tsvname);
    fs.readFile(csvSourcepath, 'utf-8', (error, csvSourceContent) => {
      this.csvSourceContentJSON = this.getJSONEquivalent(csvSourceContent);
      this.csvSourceContentJSONCopy = Object.assign({}, this.csvSourceContentJSON);
      if (error) {
        console.error("Cannot read csv source file");
        process.exit(1);
      }

      // when merging quest files handle them on different function
      const questMeta = this.questFiles.find(quest => quest[0] === this.tsvname);
      if (questMeta) {
        this.mergeQuestFile(questMeta);
      } else {
        // read sheets from translation folder
        const translationFolder = `translation/${this.tsvname}/`;
        const translatedFolder = `translated/${this.tsvname}/`;

        const translationFiles = fs.readdirSync(translationFolder);
        if (translationFiles && translationFiles.length > 0) {

          let index = 0;
          each(translationFiles, (sheet, callback) => {
            index++;
            let tsvSheetApath = `${translationFolder}/${sheet}`;
            let tsvSheetBpath = `${translatedFolder}/${sheet}`;

            this.mergeProcess(tsvSheetApath, tsvSheetBpath, callback);
          }, (err) => {
            if (!err) {
              console.log('Writing now to file');
              this.writeToFile();
            }
          });
        }
      }
    });
  }

  writeToFile() {
    let fcontent = 'ID_NO_EDIT\tORIGINAL_NO_EDIT\tTRANSLATE_EDIT\n';
    Object.keys(this.csvSourceContentJSON).forEach((hash) => {
      fcontent += `${hash}\t${this.csvSourceContentJSONCopy[hash]}\t${this.csvSourceContentJSON[hash]}\n`;
    });

    fs.writeFile(`${this.csvMergeDir}/${this.tsvname}.csv`, fcontent, (err) => {
      if (err) throw err;
      console.timeEnd(this.tsvname);
    });
  }

  mergeProcess(tsvSheetApath, tsvSheetBpath, cb = false) {
    // read tsvSheetApath
    fs.readFile(tsvSheetApath, 'utf-8', (error, tsvAcontent) => {
      if (!error) {
        let tsvALines = tsvAcontent.split(/\r?\n/g);
        fs.readFile(tsvSheetBpath, 'utf-8', (error, tsvBcontent) => {
          if (!error) {
            // break each into new line
            let tsvBLines = tsvBcontent.split(/\r?\n/g);

            // loop through the first tsv
            // and use the index reference for the tsb B
            // check if they have the same length
            if (tsvALines.length !== tsvBLines.length) {
              console.error('First and second TSV are not identical in length', tsvALines.length, tsvBLines.length);
              process.exit(1);
            }

            // start merge into one file, from the tsv csv's folder
            tsvALines.forEach((tsvALine, z) => {
              // skip the first index
              if (z !== 0) {
                // split each line by tab
                let lineAdata = tsvALine.split(this.getTabDelimiter());

                // get the line b equivalent
                let tsvBLine = tsvBLines[z];
                let lineBdata = tsvBLine.split(this.getTabDelimiter());
                let startLog = false;
                // loop through the linedata
                lineAdata.forEach((lineAText, y) => {
                  // skip the first column which is the ID's
                  if (y !== 0) {
                    let lineBText = lineBdata[y];
                    const text = lineBText;
                    if (text === 0 || text == 0 || text === '0' || text === '') {
                      // quiet
                    } else {
                      // replace any special chars first before hasing for the original text
                      lineAText = this.replaceSpecialChars(lineAText);

                      // create hash/signature of the current data line
                      const hashA = crypto.createHash('md5').update(lineAText).digest("hex");

                      // create the hash for the line B equivalent
                      let lineBText = lineBdata[y];
                      // console.log(lineBText);

                      // get the translated value from the second tsv
                      // merge this hash to the csv source file
                      if (hashA in this.csvSourceContentJSON) {
                        this.csvSourceContentJSON[hashA] = lineBText;
                        // console.log(this.csvSourceContentJSON);
                      }
                    }
                  }
                });
              }
            });

            cb && cb();
          } else {
            console.error("There is a problem reading second TSV", error);
          }
        });
      } else {
        console.error("There is a problem reading first TSV", error);
      }
    });
  }

  mergeQuestFile(questMeta) {
    // file taskdata should be available on gsheets if missing
    // change tthe sheet for specific source
    const currentQuestTsvpath = `staticdata_dec/${questMeta[0]}.dat`;
    const taskdataFileSheetPath = `translated_old/taskdata/${questMeta[1]}.tsv`;

    // read tsvSheet
    fs.readFile(taskdataFileSheetPath, 'utf-8', (error, taskdata) => {
      const taskdataJSON = this.getQuestJSONEquivalent(taskdata);
      this.parseTSVXmlAndExtract(currentQuestTsvpath, (Obj) => {
        const locations = ['Name', 'Note', 'Description'];
        if (Obj) {
          Obj.forEach(obj => {
            const id = Number(obj.Id[0]);

            // read the translated data on taskjson
            // if ID is found
            if (id in taskdataJSON) {
              let translatedData = taskdataJSON[id];
              translatedData.forEach((translated, index) => {

                // using the location as ref, get the text from the
                // real xml data
                if (locations[index] in obj) {
                  let text = String(obj[locations[index]][0]);
                  // console.log(text);
                  if (text === 0 || text == 0 || text === '0' || text === '') {
                    // console.log(text);
                  } else {
                    // create hash/signature of the text
                    const hash = crypto.createHash('md5').update(text).digest("hex");

                    // modify text, double, single, new lines stuff
                    translated = translated.replace(/{SDQ}/g, '{DQ}')

                    // this method automatically replace identical text
                    if (hash in this.csvSourceContentJSON) {
                      this.csvSourceContentJSON[hash] = translated;
                      // console.log(this.csvSourceContentJSON);
                    }
                  }
                }
              });
            }
          });
        }
      });

      console.log('Writing now to file');
      this.writeToFile();
    });
  }

  replaceSpecialChars(text) {
    return text.replace(/{DQ}/g, '\"').replace(/{NL}/g, '\\n');
  }
}

export default TsvMerger;
