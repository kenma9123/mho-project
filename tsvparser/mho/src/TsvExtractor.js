import fs from 'fs';
import Common from './Common';
import crypto from 'crypto';
import jsonfile from 'jsonfile';
import chalk from 'chalk';
import isEmpty from 'lodash/isEmpty';

class TsvExtractor extends Common {

  constructor() {
    super();
  }

  extract(sourceFile, targetDir, cb = false) {

    // source file should be a dat
    if (!~sourceFile.indexOf('.dat')) {
      console.error('Source file must be a dat file');
      return false;
    }

    // create target dir if not exist
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    let tsvname = sourceFile.split('/')[1].split('.')[0];
    let tsvpath = `${sourceFile}`;
    let hashTable = {};

    cb && console.time(`extract ${tsvname}`);

    if (!this.isFileExist(tsvpath)) {
      console.error(`ERROR: TSV File ${tsvpath} doesn't exist.`);
      return false;
    }

    // if the current tsvref has an xml tag
    // parse the xml object
    if (this.isTsvRefHasAnXML()) {
      // console.log('Parsing an XML file', tsvname);
      // read a tsv file
      const locations = ['Name', 'Note', 'Description'];
      this.parseTSVXmlAndExtract(tsvpath, (Obj) => {
        if (Obj) {
          Obj.forEach(obj => {
            // find from locations
            locations.forEach(location => {
              if (location in obj) {
                let text = String(obj[location][0]);
                // console.log(text);
                if (text === 0 || text == 0 || text === '0' || text === '') {
                  // console.log(text);
                } else {
                  // create hash/signature of the text
                  const hash = crypto.createHash('md5').update(text).digest("hex");

                  // modify text, double, single, new lines stuff
                  text = this.replaceSpecialChars(text);

                  // this method automatically replace identical text
                  hashTable[hash] = text;
                }
              }
            });
          });
        } else {
          console.error('Error handling tsv xml', tsvname);
          process.exit(1);
        }
      });
    } else {
      // parse non xml tsv
      this.getLinedataOfEachSheets(tsvpath, (lines) => {
        let sheetName = lines[0];
        // console.log(sheetName);

        // tsv reference json
        const sheetOpt = this.getTSVRefSheetsOptions(sheetName);
        const sheetXML = ('xml' in sheetOpt && !isEmpty(sheetOpt.xml)) ? sheetOpt.xml : false;
        const columns = (!isEmpty(sheetOpt.cols)) ? sheetOpt.cols : [];
        if (columns.length > 0) {
          // console.log(sheetName);

          lines.forEach((line, x) => {
            // we start after the sheet ID and table header
            // index 0 is sheet ID
            // index 1 is table header
            // we only need index 2 and 3 for most file
            if (x > 1) {
              let linedata = line.split(this.getTabDelimiter());

              // read text from a column reference
              columns.forEach((col) => {
                let text = linedata[Number(col) - 1];
                if (!sheetXML) {
                  // do not add empty text
                  if (text === 0 || text == 0 || text === '0' || text === '') {
                    // console.log(text);
                  } else {
                    // create hash/signature of the text
                    const hash = crypto.createHash('md5').update(text).digest("hex");

                    // modify text, double, single, new lines stuff
                    text = this.replaceSpecialChars(text);

                    // this method automatically replace identical text
                    hashTable[hash] = text;
                  }
                } else {
                  this.parseXmlStringAndExtract(JSON.parse(text), (result) => {
                    // find from locations
                    sheetXML.forEach(location => {
                      if (location in result) {
                        let text = String(result[location][0]);
                        // console.log(text);
                        if (text === 0 || text == 0 || text === '0' || text === '') {
                          // console.log(text);
                        } else {
                          // create hash/signature of the text
                          const hash = crypto.createHash('md5').update(text).digest("hex");

                          // modify text, double, single, new lines stuff
                          text = this.replaceSpecialChars(text);

                          // this method automatically replace identical text
                          hashTable[hash] = text;
                        }
                      }
                    });
                  });
                }
              });
            }
          });
        }
      }, () => {
        console.error(`Error opening tsv file: ${tsvpath}`);
      });
    }

    let fcontent = 'ID_NO_EDIT\tORIGINAL_NO_EDIT\tTRANSLATE_EDIT\tCOMMENTS\n';
    Object.keys(hashTable).forEach((hash) => {
      let comments = '<EMPTY>';
      fcontent += `${hash}\t${hashTable[hash]}\t${hashTable[hash]}\t${comments}\n`;
    });

    fs.writeFile(`${targetDir}/${tsvname}.csv`, fcontent, (err) => {
      if (err) throw err;
        if (cb) {
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(`extract ${tsvname}`);
          cb && cb();
        } else {
          console.log(`${tsvname} extraced`);
        }
    });
  }

  replaceSpecialChars(text) {
    return text.replace(/\"/g, '{DQ}').replace(/\\n/g, '{NL}');
  }
}

export default TsvExtractor;
