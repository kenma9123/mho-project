import aesjs from 'aes-js';
import fs from 'fs';
import chalk from 'chalk';
import readline from 'readline';
import Common from './Common';

const key1 = [0x01, 0x09, 0x08, 0x00];
const key2 = [0x00, 0x02, 0x01, 0x06];
const key3 = [0x0A, 0x0B, 0x0C, 0x0D];
const key4 = [0x0E, 0x0F, 0x10, 0x11];

const key = new Buffer([...key1, ...key2, ...key3, ...key4]);

class Crypto {

  constructor() {
    this.crypto = new aesjs.ModeOfOperation.ecb(key);

    this.chunks = [];
    this.block = [];
    this.blockIndex = 0;
    this.blockSize = 16;

    this.encDir = `staticdata_enc/`;
    this.encReadyDir = `staticdata_ready/`;
    this.decDir = `staticdata_dec/`;
  }

  encrypt(sourceFile, targetFile, cb = false) {
    this.reset();

    // get tsv name
    const tsvname = sourceFile.split('/')[1].replace('.dat', '');

    // create enc ready dir if not exist
    if (!fs.existsSync(this.encReadyDir)){
      fs.mkdirSync(this.encReadyDir);
    }

    // remove enc existed file
    if (fs.existsSync(targetFile)){
      fs.unlink(targetFile, () => {});
    }

    // start enc
    console.time(`encrypt ${tsvname}`);
    var readStream = fs.createReadStream(sourceFile);
    fs.readFile(sourceFile, (err, chunks) => {
      if (err) throw err;

      // set chunks
      this.setChunks(chunks);

      let chunkSize = this.getChunksSize();

      // adjust size, it must be
      // divisible by 16bytes
      while (chunkSize % 16 != 0) {
        chunkSize++;
      }

      // final buffer
      let bufferBlock = Buffer.alloc(chunkSize);
      let bufferBlockOffset = 0;

      // write the header
      const firstsig = 0x00000010;
      const secondsig = 0x00000043;
      const thirdsig = chunkSize;
      const lastsig = 0x00000000;

      bufferBlock.writeInt32LE(firstsig, 0);
      bufferBlock.writeInt32LE(secondsig, 4);
      bufferBlock.writeInt32LE(thirdsig, 8);
      bufferBlock.writeInt32LE(lastsig, 12);

      // after writing headers, start at buffer offset to 16
      // and start writing the encrypted bytes
      bufferBlockOffset = 16;

      for (var x = 0; x < chunkSize; x++) {
        // for adjusted chunk size
        // if its not valid chunk, set it to -1
        var chunk = (typeof chunks[x] !== 'undefined') ? chunks[x] : -1;
        if (chunk == -1) {
          chunk = 0;
        }

        // set byte to current block
        this.setBlockByte(chunk);
        if (this.getBlockIndex() % this.getBlockSize()) {
          // next
          continue;
        } else {
          // copy decrypted bytes to final buffer
          var encryptedBytes = this.crypto.decrypt(this.getBlockBuffer());
          for (let i = 0; i < encryptedBytes.length; i++) {
            encryptedBytes.copy(bufferBlock, bufferBlockOffset++, i);
          }

          // reset block index
          this.resetBlockIndex();
        }
      }

      fs.writeFile(targetFile, bufferBlock, 'binary', (err) => {
        if (err) throw err;
        process.stdout.write(chalk.green('OK') + ' ');
        console.timeEnd(`encrypt ${tsvname}`);
        cb && cb();
      });
    });
  }

  directDecrypt(sourceFile) {
    return new Promise((resolve, reject) => {
      try {
        this._decrypt(sourceFile, resolve);
      } catch(e) {
        reject(e);
      }
    });
  }

  _decrypt(sourceFile, cb = false) {
    var readStream = fs.createReadStream(sourceFile);
    fs.readFile(sourceFile, (err, chunks) => {
      if (err) throw err;

      // set chunks
      this.setChunks(chunks);

      // final buffer
      let bufferBlock = Buffer.alloc(this.getChunksSize());
      let bufferBlockOffset = 0;
      let startFromOffset = 16;

      for (var x = startFromOffset; x < this.getChunksSize(); x++) {
        var chunk = chunks[x];
        if (chunk == -1) {
          chunk = 0;
        }

        // set byte to current block
        this.setBlockByte(chunk);
        if (this.getBlockIndex() % this.getBlockSize()) {
          // next
          continue;
        } else {
          // copy decrypted bytes to final buffer
          var decryptedBytes = this.crypto.encrypt(this.getBlockBuffer());
          for (let i = 0; i < decryptedBytes.length; i++) {
            decryptedBytes.copy(bufferBlock, bufferBlockOffset++, i);
          }

          // reset block index
          this.resetBlockIndex();
        }
      }

      cb && cb(bufferBlock);
    });
  }

  decrypt(sourceFile, targetFile, cb = false) {
    this.reset();

    // get tsv name
    const tsvname = sourceFile.split('/')[1].replace('.dat', '');

    // create dec dir if not exist
    if (!fs.existsSync(this.decDir)){
      fs.mkdirSync(this.decDir);
    }

    // remove dec existed file
    if (fs.existsSync(targetFile)){
      fs.unlink(targetFile, () => {});
    }

    // start dec
    console.time(`decrypt ${tsvname}`);
    this._decrypt(sourceFile, (bufferBlock) => {
      fs.writeFile(targetFile, bufferBlock, 'binary', (err) => {
        if (err) throw err;
        process.stdout.write(chalk.green('OK') + ' ');
        console.timeEnd(`decrypt ${tsvname}`);
        cb && cb();
      });
    });
  }

  reset() {
    this.chunks = [];
    this.block = [];
    this.blockIndex = 0;
    this.blockSize = 16;
  }

  ////////////////////
  // HELPER METHODS //
  ////////////////////

  setChunks(chunks) {
    this.chunks = chunks;
  }

  getChunksSize() {
    return this.chunks.length || 0;
  }

  getBlock() {
    return this.block;
  }

  getBlockBuffer() {
    return new Buffer(this.block);
  }

  setBlockByte(byte) {
    this.block[this.blockIndex++] = byte;
  }

  getBlockIndex() {
    return this.blockIndex;
  }

  setBlockIndex(index) {
    this.blockIndex = index;
  }

  resetBlockIndex() {
    this.setBlockIndex(0);
  }

  getBlockSize() {
    return this.blockSize;
  }

}

export default Crypto;
