import fs from 'fs';
import {google} from 'googleapis';
import readline from 'readline';
import open from 'open';

class Spreadsheet {
  constructor() {
    this.credentials = {
      "client_id": "534972241672-ihakbmgqvv0ra16649r9tcftl7vqlam8.apps.googleusercontent.com",
      "project_id": "teamhd-150017",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://accounts.google.com/o/oauth2/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_secret": "98jiiwtHrL9qQLJH_MNQcbMk",
      "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob", "http://localhost"]
    };

    // If modifying these scopes, delete your previously saved credentials
    this.SCOPES = [
      'https://www.googleapis.com/auth/spreadsheets.readonly',
      'https://www.googleapis.com/auth/drive'
    ];
    this.TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';
    this.TOKEN_PATH = this.TOKEN_DIR + 'googleapis.com-nodejs-teamhd.json';

  }

  /**
   * Read a spreadsheet
   * https://developers.google.com/sheets/quickstart/nodejs
   */
  read(spreadsheetId = false, filename) {

    if (!spreadsheetId) {
      console.error("No spreadsheet id provided");
      process.exit(1);
    }

    return new Promise((resolve, reject) => {
      try {
        // set credentials now
        this.authorize(auth => {
          var service = google.sheets({
            version: 'v4',
            auth
          });

          service.spreadsheets.values.get({
            spreadsheetId,
            range: filename,
          }, (err, result) => {
            if (err) {
              reject('The API returned an error: ' + err);
              return;
            }

            let response = result.data;

            if (!response) {
              console.log(err);
              reject('No response with error message', err.errors[0].message);
              return;
            }



            var rows = ('values' in response) ? response.values : [];
            if (rows.length == 0) {
              reject('No data found.');
            } else {
              resolve(rows);
            }
          });
        });
      } catch (e) {

      }
    });
  }

  /**
   * Upload a file/csv
   * https://developers.google.com/drive/v3/web/quickstart/nodejs
   */
  upload(filepath, name, folderID = '0B2-sIhD0bUJdWDh6MmVsZmJiQjg') {
    return new Promise((resolve, reject) => {
      // set credentials now
      this.authorize(auth => {
        var service = google.drive({
          version: 'v3',
          auth
        });

        service.files.create({
          resource: {
            name,
            parents: [ folderID ],
            mimeType: 'application/vnd.google-apps.spreadsheet'
          },
          media: {
            mimeType: 'text/csv',
            body: fs.readFileSync(filepath, 'utf-8')
          },
          fields: 'id'
        }, function(err, result) {
          if(err) {
            // Handle error
            reject(err);
          } else {
            let file = result.data;
            resolve(file);
          }
        });
      });
    });
  }

  update(filepath, metadata, folderID = '0B2-sIhD0bUJdWDh6MmVsZmJiQjg') {
    return new Promise((resolve, reject) => {
      // set credentials now
      this.authorize(auth => {
        var service = google.drive({
          version: 'v3',
          auth
        });

        service.files.update({
          fileId: metadata.id,
          addParents: folderID,
          resource: {
            name: metadata.name,
            mimeType: 'application/vnd.google-apps.spreadsheet'
          },
          media: {
            mimeType: 'text/csv',
            body: fs.readFileSync(filepath, 'utf-8')
          }
        }, function(err, result) {
          if(err) {
            // Handle error
            reject(err);
          } else {
            let file = result.data;
            resolve(file);
          }
        });
      });
    });
  }

  downloadFolder(folderID = '0B2-sIhD0bUJdWDh6MmVsZmJiQjg') {
    return new Promise((resolve, reject) => {
      // set credentials now

      this.authorize(auth => {
        var service = google.drive({
          version: 'v3',
          auth
        });

        service.files.list({
          fields: 'nextPageToken, files(id, name, parents, mimeType, modifiedTime)',
          q: `trashed=false and '${folderID}' in parents`,
          pageSize: 1000,
          orderBy: 'name'
          // q: `trashed=false and not name contains 'ui.csv'`
        }, function(err, response) {
          if (err) {
            reject('The API returned an error: ' + err);
            return;
          }

          let { data } = response;
          let files = data.files;
          // console.log(files);
          if (files.length == 0) {
            reject('No files found.');
          } else {
            resolve(files);
          }
        });
      });
    });
  }

  // downloadDocument(targetPath, fileId, mimeType = 'text/csv') {
  //   return new Promise((resolve, reject) => {
  //     // set credentials now
  //     this.authorize(auth => {
  //       var service = google.drive('v2');
  //       // var dest = fs.createWriteStream(targetPath);
  //       service.files.export({
  //         auth,
  //         fileId,
  //         mimeType
  //       }, function(err, response) {
  //         if (err) {
  //           reject('The API returned an error: ' + err);
  //           return;
  //         }

  //         resolve(response);
  //       });
  //     });
  //   });
  // }

  downloadDocumentTest() {
    // set credentials now
    this.authorize(auth => {
      var service = google.drive({
        version: 'v3',
        auth
      });

      var dest = fs.createWriteStream('downloaded_translations/resume.pdf');
      var fileId = '1UuX5rUWr_DG04Oupaq1z9lmmcnT183UXLw4Z0y2tuVY';
      service.files.get({
        fileId
      })
      .on('end', function() {
        console.log('Done');
      })
      .on('error', function(err) {
        console.log('Error during download', err);
      })
      .pipe(dest);
    });
  }

  /**
   * Create an OAuth2 client with the given credentials, and then execute the
   * given callback function.
   *
   * @param {Object} credentials The authorization client credentials.
   * @param {function} callback The callback to call with the authorized client.
   */
  authorize(callback) {
    let { client_id, client_secret, redirect_uris } = this.credentials;
    var oauth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(this.TOKEN_PATH, (err, tokens) => {
      if (err) {
        this.getNewToken(oauth2Client, callback);
      } else {
        oauth2Client.setCredentials(JSON.parse(tokens));
        callback(oauth2Client);
      }
    });
  }

  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   *
   * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
   * @param {getEventsCallback} callback The callback to call with the authorized
   *     client.
   */
  getNewToken(oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: this.SCOPES
    });
    console.log('Authorize this app from your browser');
    try {
      open(authUrl);
    } catch (e) {
      // console.error('Error:', e.getMessage());
    }
    console.log('In case of errors, visit the following link');
    console.log(authUrl);
    console.log('');
    var rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    rl.question('Enter the code from that page here: ', (code) => {
      rl.close();
      oauth2Client.getToken(code, (err, tokens) => {
        if (err) {
          console.log('Error while trying to retrieve access token', err);
          return;
        }

        this.storeToken(tokens);
        oauth2Client.setCredentials(tokens);
        callback(oauth2Client);
      });
    });
  }

  /**
   * Store token to disk be used in later program executions.
   *
   * @param {Object} token The token to store to disk.
   */
  storeToken(tokens) {
    try {
      fs.mkdirSync(this.TOKEN_DIR);
    } catch (err) {
      if (err.code != 'EEXIST') {
        throw err;
      }
    }
    fs.writeFileSync(this.TOKEN_PATH, JSON.stringify(tokens));
    console.log('Token stored to ' + this.TOKEN_PATH);
  }
}

export default Spreadsheet;
